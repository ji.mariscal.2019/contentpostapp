from webapp import webapp
from urllib.parse import parse_qs

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Recurso: </label>
        <input type="text" name="recurso" required>
      </div>
      <div>
        <label>Contenido: </label>
        <input name="contenido" required></input>
      </div>
      <div>
        <input type="submit" value="Guardar">
      </div>
    </form>
"""


class ContentApp(webapp):
    content = {'/': 'Pagina de inicio',
               '/pagina': 'Página sin contenido',
               '/adios': 'Adios Mundo Cruel'}

    def parse(self, request):
        method, resource = request.split(' ', 2)[:2]
        body = request.split('\r\n\r\n', 1)[-1]
        return method, resource, body

    def process(self, parsed_request):
        method, resource, body = parsed_request
        if method == 'GET':
            if resource in self.content:
                code = '200 OK'
                html = "<html><body><h1>" + self.content[resource] + "</h1></body></html>"
            else:
                code = '404 Not Found'
                html = "<html><body> No encontrado. Agrega tu mismo el recurso:"
                html += FORM
                html += "</body></html>"
            return code, html
        elif method == 'POST':
            form_data = parse_qs(body)
            resource = form_data['recurso'][0]
            if resource[0] != '/':
                resource = '/' + resource
            content = form_data['contenido'][0]
            self.content[resource] = content  # Agregar el contenido al diccionario
            code = '200 OK'
            html = "<html><body><h1>Contenido actualizado:</h1>"
            html += "<p>Recurso: {}</p>".format(resource)
            html += "<p>Contenido: {}</p>".format(content)
            html += "</body></html>"
            return code, html


if __name__ == "__main__":
    ContentApp("localhost", 8080)
